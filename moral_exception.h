#ifndef MORAL_EXCEPTION_H
#define MORAL_EXCEPTION_H

#include <stdexcept>
#include "toerr.h"

namespace sins {

    struct moral_exception: public std::runtime_error {

        using std::runtime_error::runtime_error;

        virtual ~moral_exception() = default;

    };

}

#endif // MORAL_EXCEPTION_H
