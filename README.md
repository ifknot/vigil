# README #

![no expect](https://cldup.com/ZA2c0vWxg9.png)

It's Vigil for C++ but with the chance of redemption!

Bob Nystrom has created the excellent [Vigil programming language](https://github.com/munificent/vigil) that auto-deletes any deviant code from the code base! Vigil++ is a denomination that offers redemption! Whilst it does deliver `implore` and `swear` macros, it does not adhere to the extreme run time vigilance of Vigil by deleting code but rather throws a `moral-exception` and a frowny face ಠ_ಠ  as appropriate admonishment! But whilst you will be chastened, you will also be given the chance of redemption  by being able to recover from the `moral-exception`. 

### Why Vigil++? ###

* Because eternal moral vigilance is no laughing matter.
* Because the ideas behind Vigil, those of [Design by Contract](https://en.wikipedia.org/wiki/Design_by_contract) are valuable but paying the price of a whole new language is too damn high!
* Because the typical C++ idioms for design by contract by using `assert` via `assert.h` or `cassert` are really only for debug release of C/C++ codebase and are intended to be removed once `NDEBUG` is defined. Further, because `assert` halts the entire execution there is no chance for redemption.
* Using Vigil++ offers all the benefits of Vigil with the `implore` and `swear` commands but offers the chance of **redemption** by throwing a `moral-exception` - every such excpetion message as pre-fixed by a withering frown as suitable admonishment for your misdeeds ಠ_ಠ
* Because paying the price of code deletion as punsihment for breaking the contract is too damn medieval!
* Go forward and **program by contract**.
* Version 0.1
* [Learn about Assertions vs Exceptions](https://web.archive.org/web/20090707025230/http://www.ddj.com/blog/cppblog/archives/2007/07/assertions_vers.html)

### Licence ###
`Vigil++`is free (as in beer) software and is licensed under the [GNU Lesser General Public License (LGPL) version 3](https://www.gnu.org/licenses/lgpl-3.0.en.html).


###Syntax and semantics###

`Vigil++` is very similar to the original [Vigil](https://github.com/munificent/vigil) but with the doctrinal schism that your code is _potentially redeemable_ but in order for this to happen you must catch the `moral-exception` which will be automatically thrown for you when your code is _wrong_ and must be frowned upon!

Infinitely more important than mere syntax and semantics are its addition of supreme moral vigilance. This is similar to contracts, but less legal and more medieval - but now with added redemption!

###The `implore` statement to beseech your needs##

Often, a C++ class member function will require that parameters have certain desirable properties or _pre-conditions_. A member function with access to `vigil.h` can state what it requires by using `implore`:

```cpp
#include "vigil.h"
#include <math.h> 

float square_root(float n) {
    implore(n >= 0.0);
    return sqrt(n);
}
```
If a caller fails to provide valid arguments, it is wrong and must be ~~punished~~ frowned upon ಠ_ಠ and a `moral-exception` is thrown to offer a chance of redemption.

###The `swear` statement to state what you provide in return###

If a good caller meets its obligations, the onus is thus on you to fulfill your end of the bargain. You can state the oaths (post-conditions) that you promise to uphold using swear:

```cpp
#include "vigil.h"

int fib(int n) {
	int result;
  	if (n < 2) {
   		result = n;
   	} else {
       result = fib(n - 1) + fib(n - 2);
	}
   //fib() never returns negative number.
   swear(result >= 0);
   return result;
}
```
If a function/member function fails to uphold what it has sworn to do, it is wrong and must be ~~punished~~ frowned upon ಠ_ಠ and a `moral-exception` is thrown to offer a chance of redemption.

###Unhandled exceptions###

```
terminating with uncaught exception of type
sins::moral-exception: 
ಠ_ಠ square_root is wrong and has failed to implore that n >= 0.0
```

It goes without saying that any wrong code hoping for redemption _must_ catch `moral-exception` and failing to do so will result in your eternal moral shame and you shall be frowned upon forever!

### Contribution guidelines ###

* __Writing tests__ All tests are welcome _but_ must be submitted in the [Bandit](http://banditcpp.org/) unit testing framework format.
* __Code review__ All _constructive_ code review is welcome

### Who do I talk to? ###

* Repo owner: https://bitbucket.org/ifknot/
* Support: [@ifknot](https://twitter.com/ifknot)