#include <bandit/bandit.h>
#include <math.h>
#include "vigil.h"

using namespace bandit;

float square_root(float n) {
    implore(n >= 0.0);
    return sqrt(n);
}

int fib(int n) {
    int result;
    if (n < 2) {
        result = n;
    } else {
       result = fib(n - 1) + fib(n - 2);
       result = result == 34 ? -1 : result; //so much wrongness!
    }
   //fib() never returns negative number.
   swear(result >= 0);
   return result;
}

go_bandit([](){

    describe("test moral vigilence", [](){

        describe("test morality", [](){

            struct bad {

                void operator()(){
                    throw sins::moral_exception(err::FROWN);
                }

            };

            bad wrong;

            it("should frown upon your sin::moral_exception.", [&](){
                AssertThrows(sins::moral_exception, wrong());
                Assert::That(LastException<sins::moral_exception>().what(), Is().Containing(err::FROWN));
            });

            it("should fail to implore and offer up square_root for redemption.", [&](){
                AssertThrows(sins::moral_exception, square_root(-1));
                Assert::That(LastException<sins::moral_exception>().what(), Is().Containing("n >= 0.0"));
            });

            it("should fail to swear fib >= 0 and offer up fib for redemption.", [&](){
                AssertThrows(sins::moral_exception, fib(9));
                Assert::That(LastException<sins::moral_exception>().what(), Is().Containing("result >= 0"));
            });

        });

    });

});
