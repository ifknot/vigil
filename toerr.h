#ifndef TOERR_H
#define TOERR_H

#include <string>

namespace err {

    const std::string FROWN = "ಠ_ಠ ";
    const std::string WRONG = " is wrong and has failed";
    const std::string IMPLORE =     " to implore that ";
    const std::string SWEAR =     " to swear that ";

}

#endif // TOERR_H
