#include <iostream>
#include <bandit/bandit.h>

int main(int argc, char* argv[]) {

    std::cout << "Running unit tests..." << std::endl;
    return bandit::run(argc, argv);

}
