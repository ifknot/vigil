QT += core
QT -= gui

CONFIG += c++11

TARGET = vigil
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    test_vigil.cpp

DISTFILES += \
    README.md

HEADERS += \
    moral_exception.h \
    toerr.h \
    vigil.h
