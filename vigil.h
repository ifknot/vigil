#ifndef VIGIL_H
#define VIGIL_H

#include "moral_exception.h"

/**
 * The implore statement to beseech your needs.
 *
 * <p>Often, a C++ class member function will require that parameters have certain desirable properties or pre-conditions. A member function with access to vigil.h can state what it requires by using implore:</p>
 *
 * <code>
 * #include "vigil.h"
 * #include <math.h>
 *
 * float square_root(float n) {
 *   implore(n >= 0.0);
 *   return sqrt(n);
 * }
 * </code>
 * <p>If a caller fails to provide valid arguments, it is wrong and must be ~~punished~~ frowned upon ಠ_ಠ and a moral-excpetion is thrown to offer a chance of redemption.</p>
 */
#define implore(TEST) if(!(TEST)) { throw sins::moral_exception(err::FROWN + __func__ + err::WRONG + err::IMPLORE + #TEST); }


/**
 * The swear statement to state what you provide in return.
 *
 * <p>If a good caller meets its obligations, the onus is thus on you to fulfill your end of the bargain. You can state the oaths (post-conditions) that you promise to uphold using swear:</p>
 *
 * <code>
 * #include "vigil.h"
 *
 * int fib(int n) {
 *     int result;
 *     if n < 2 {
 *         result = n;
 *     } else {
 *        result = fib(n - 1) + fib(n - 2);
 *     }
 *     //fib() never returns negative number.
 *     swear(result >= 0);
 *     return result;
 * }
 * </code>
 *
 * <p>If a function/member function fails to uphold what it has sworn to do, it is wrong and must be ~~punished~~ frowned upon ಠ_ಠ and a moral-excpetion is thrown to offer a chance of redemption.</p>
 */
#define swear(TEST) if(!(TEST)) { throw sins::moral_exception(err::FROWN + __func__ + err::WRONG + err::SWEAR + #TEST); }

#endif // VIGIL_H
